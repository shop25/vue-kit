# app

## Project setup
```
add line to package.json 

"vue-kit": "git+https://git@bitbucket.org:s25/vue-kit.git#<last commit hash>"
```

### Usage
```
import { Spinner } from 'vue-kit/components' (see all components on bitbucket)
import { BaseStore, DynamicModule } from 'vue-kit/decorators'
import { ServerResponse, ToolbarLink } from 'vue-kit/dto'
import { DateService, NotifyService } from 'vue-kit/services'
import { LayoutStore } from 'vue-kit/stores'
import 'kit/directives'
```

