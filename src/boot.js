import Vue from 'vue'
import Loading from '@/common/components/Loading.vue'
import Spinner from '@/common/components/Spinner.vue'
import PageHeader from '@/common/components/PageHeader.vue'
import { Notify } from 'quasar'
import ToggleBtn from '@/common/components/ToggleBtn.vue'
import ControlPanel from '@/common/components/ControlPanel.vue'
import FieldInput from '@/common/components/FieldInput.vue'
import FieldSelect from '@/common/components/FieldSelect.vue'
import FieldOptions from '@/common/components/FieldOptions.vue'
import Sidebar from '@/common/components/Sidebar.vue'
import { addDirectives } from './common/directives'
import Router from './router/index'
import BtnSecondary from '@/common/components/BtnSecondary.vue'

addDirectives(Router)

Vue.config.productionTip = false
Vue.component('loading', Loading)
Vue.component('spinner', Spinner)
Vue.component('sidebar', Sidebar)
Vue.component('page-header', PageHeader)
Vue.component('toggle-btn', ToggleBtn)
Vue.component('control-panel', ControlPanel)
Vue.component('field-input', FieldInput)
Vue.component('field-select', FieldSelect)
Vue.component('field-options', FieldOptions)
Vue.component('btn-secondary', BtnSecondary)

Notify.setDefaults({
  position: 'bottom-right',
  message: '',
  color: 'positive',
  textColor: 'white',
  timeout: 1000,
})
