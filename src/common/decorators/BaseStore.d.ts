import { VuexModule } from 'vuex-module-decorators';
declare type ModelId = string | number;
declare type BaseModel = {
    [key: string]: any;
    id: ModelId | null;
};
export default class BaseStore<Model extends BaseModel> extends VuexModule {
    models: Model[];
    modelId: ModelId | null;
    readonly model: Model | null;
    readonly modelsById: any;
    setModels(models: Model[]): void;
    selectModel(modelId: ModelId | null): void;
    addModel(model: Model, toEnd?: boolean): void;
    updateModelInList(changedModel: Model): void;
    removeModel(modelId: ModelId): void;
}
export {};
