"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var vuex_module_decorators_1 = require("vuex-module-decorators");
var BaseStore = /** @class */ (function (_super) {
    __extends(BaseStore, _super);
    function BaseStore() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.models = [];
        _this.modelId = null;
        return _this;
    }
    Object.defineProperty(BaseStore.prototype, "model", {
        get: function () {
            var _this = this;
            return this.models.find(function (model) { return model.id === _this.modelId; }) || null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseStore.prototype, "modelsById", {
        get: function () {
            return this.models.reduce(function (result, model) {
                result[model.id] = model;
                return result;
            }, {});
        },
        enumerable: true,
        configurable: true
    });
    BaseStore.prototype.setModels = function (models) {
        this.models = models;
    };
    BaseStore.prototype.selectModel = function (modelId) {
        this.modelId = modelId;
    };
    BaseStore.prototype.addModel = function (model, toEnd) {
        toEnd ? this.models.push(model) : this.models.unshift(model);
    };
    BaseStore.prototype.updateModelInList = function (changedModel) {
        var index = this.models.findIndex(function (model) {
            return model.id === changedModel.id;
        });
        this.models.splice(index, 1, changedModel);
    };
    BaseStore.prototype.removeModel = function (modelId) {
        this.models.splice(this.models.findIndex(function (model) {
            return model.id === modelId;
        }), 1);
    };
    __decorate([
        vuex_module_decorators_1.Mutation
    ], BaseStore.prototype, "setModels");
    __decorate([
        vuex_module_decorators_1.Mutation
    ], BaseStore.prototype, "selectModel");
    __decorate([
        vuex_module_decorators_1.Mutation
    ], BaseStore.prototype, "addModel");
    __decorate([
        vuex_module_decorators_1.Mutation
    ], BaseStore.prototype, "updateModelInList");
    __decorate([
        vuex_module_decorators_1.Mutation
    ], BaseStore.prototype, "removeModel");
    return BaseStore;
}(vuex_module_decorators_1.VuexModule));
exports["default"] = BaseStore;
