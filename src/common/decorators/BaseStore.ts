import { Mutation, VuexModule } from 'vuex-module-decorators'

type ModelId = string | number

type BaseModel = {
  [key: string]: any
  id: ModelId | null
}

export default class BaseStore<Model extends BaseModel> extends VuexModule {
  models: Model[] = []
  modelId: ModelId | null = null

  get model(): Model | null {
    return this.models.find(model => model.id === this.modelId) || null
  }

  get modelsById(): any {
    return this.models.reduce((result: any, model) => {
      result[model.id as number] = model
      return result
    }, {})
  }

  @Mutation
  public setModels(models: Model[]) {
    this.models = models
  }

  @Mutation
  public selectModel(modelId: ModelId | null) {
    this.modelId = modelId
  }

  @Mutation
  public addModel(model: Model, toEnd?: boolean) {
    toEnd ? this.models.push(model) : this.models.unshift(model)
  }

  @Mutation
  public updateModelInList(changedModel: Model) {
    const index = this.models.findIndex(model => {
      return model.id === changedModel.id
    })

    this.models.splice(index, 1, changedModel)
  }

  @Mutation
  public removeModel(modelId: ModelId) {
    this.models.splice(
      this.models.findIndex(model => {
        return model.id === modelId
      }),
      1
    )
  }
}
