import { VuexModule } from 'vuex-module-decorators';
import { Store } from 'vuex';
declare type ConstructorOf<M> = new (...rest: any[]) => M;
export default function <S>(RootStore: Store<S>): <M extends VuexModule<ThisType<any>, any>, C extends ConstructorOf<M>>(constructor: C) => void | C;
export {};
