"use strict";
exports.__esModule = true;
var vuex_module_decorators_1 = require("vuex-module-decorators");
var modulesCount = 0;
function default_1(RootStore) {
    return function DynamicModule(constructor) {
        return vuex_module_decorators_1.Module({
            dynamic: true,
            store: RootStore,
            name: process.env.NODE_ENV === 'production'
                ? "component" + modulesCount++
                : constructor.name,
            namespaced: true
        })(constructor);
    };
}
exports["default"] = default_1;
