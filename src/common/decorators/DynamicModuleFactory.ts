import { Module, VuexModule } from 'vuex-module-decorators'
import { Store } from 'vuex'

type ConstructorOf<M> = new (...rest: any[]) => M
let modulesCount = 0

export default function<S>(RootStore: Store<S>) {
  return function DynamicModule<
    M extends VuexModule,
    C extends ConstructorOf<M>
  >(constructor: C): C | void {
    return Module({
      dynamic: true,
      store: RootStore,
      name:
        process.env.NODE_ENV === 'production'
          ? `component${modulesCount++}`
          : constructor.name,
      namespaced: true,
    })(constructor)
  }
}
