"use strict";
exports.__esModule = true;
var vue_1 = require("vue");
function addDirectives(router) {
    var map = new WeakMap();
    vue_1["default"].directive('to', {
        bind: function (el, binding, vnode) {
            var func = function () {
                var toHref = router.resolve(binding.value).href;
                var currentHref = router.resolve(router.currentRoute.fullPath).href;
                if (currentHref !== toHref) {
                    router.push(binding.value);
                }
            };
            map.set(el, func);
            el.addEventListener('click', func);
        },
        unbind: function (el) {
            el.removeEventListener('click', map.get(el));
        }
    });
}
exports.addDirectives = addDirectives;
