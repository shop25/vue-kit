import Vue from 'vue'
import { VueRouter } from 'vue-router/types/router'

export function addDirectives(router: VueRouter) {
  const map = new WeakMap()

  Vue.directive('to', {
    bind: function(el, binding, vnode) {
      const func = () => {
        const toHref = router.resolve(binding.value).href
        const currentHref = router.resolve(router.currentRoute.fullPath).href

        if (currentHref !== toHref) {
          router.push(binding.value)
        }
      }

      map.set(el, func)
      el.addEventListener('click', func)
    },
    unbind: function(el) {
      el.removeEventListener('click', map.get(el))
    },
  })
}
