"use strict";
exports.__esModule = true;
var vue_1 = require("vue");
var sortablejs_1 = require("sortablejs");
var sortableMap = new WeakMap();
var emit = function (vnode) {
    var _a;
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    if (vnode.data && vnode.data.on) {
        var handlers = vnode.data.on;
        if ('reorder' in handlers) {
            ;
            (_a = (handlers.reorder && { fns: Function })).fns.apply(_a, args);
        }
    }
};
var onSort = function (element, vnode) { return function (event) {
    if (!vnode || !vnode.data || !vnode.data.attrs) {
        return;
    }
    var array = vnode.data.attrs.sorting;
    if (!array) {
        return;
    }
    var target = array[event.oldIndex];
    var deleted = array.splice(event.oldIndex, 1);
    array.splice(event.newIndex, 0, deleted[0]);
    emit(vnode, {
        target: target,
        sorted: array,
        oldIndex: event.oldIndex,
        newIndex: event.newIndex
    });
}; };
function directiveSortable() {
    vue_1["default"].directive('sortable', {
        bind: function (el, binding, vnode) {
            var config = binding.value || {};
            config.onSort = onSort(el, vnode);
            if (!('animation' in config)) {
                config.animation = 200;
            }
            sortableMap.set(el, sortablejs_1["default"].create(el, config));
        },
        update: function (el, binding, vnode) {
            var sortable = sortableMap.get(el);
            sortable.option('onSort', onSort(el, vnode));
        }
    });
}
exports.directiveSortable = directiveSortable;
