export default interface ServerResponse {
  status: 'success' | 'error'
  message: string
}
