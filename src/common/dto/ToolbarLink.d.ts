import { RawLocation } from 'vue-router';
export default interface ToolbarLink {
    route: RawLocation;
    label: string;
}
