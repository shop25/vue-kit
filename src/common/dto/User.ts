export default interface User {
  id: number
  positionId: number | null
  avatar: string | null
  groupId: number | null
  timezoneId: number | null
  authMethodId: number
  firstName: string
  lastName: string
  phone: string
  password: string
  personnelNumber: string
  enabled: boolean
  createdAt: string
  positionName: string
}
