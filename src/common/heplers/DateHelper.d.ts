declare class DateHelper {
    format(timestamp: number): string;
}
declare const _default: DateHelper;
export default _default;
