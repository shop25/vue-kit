"use strict";
exports.__esModule = true;
var date_fns_1 = require("date-fns");
var month = [
    'янв',
    'фев',
    'мар',
    'апр',
    'май',
    'июн',
    'июл',
    'авг',
    'сен',
    'окт',
    'ноя',
    'дек',
];
var DateHelper = /** @class */ (function () {
    function DateHelper() {
    }
    DateHelper.prototype.format = function (timestamp) {
        if (!timestamp) {
            return '';
        }
        var currentDate = new Date();
        var targetDate = new Date();
        targetDate.setTime(timestamp * 1000);
        var diffDays = date_fns_1.differenceInDays(currentDate, targetDate);
        var isCurrentYear = date_fns_1.differenceInYears(currentDate, targetDate) === 0;
        if (diffDays === 0) {
            return getTimeAsString(targetDate);
        }
        if (diffDays > 0 && isCurrentYear) {
            return getMonthAsString(targetDate) + ", " + getTimeAsString(targetDate);
        }
        return getMonthAsString(targetDate) + ", " + getYearAsString(targetDate);
    };
    return DateHelper;
}());
function getTimeAsString(date) {
    return date.getHours() + ":" + date.getMinutes();
}
function getMonthAsString(date) {
    return date.getDate() + " " + month[date.getMonth()];
}
function getYearAsString(date) {
    return date.getFullYear().toString();
}
exports["default"] = new DateHelper();
