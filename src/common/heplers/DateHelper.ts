import { differenceInDays, differenceInYears } from 'date-fns'

const month = [
  'янв',
  'фев',
  'мар',
  'апр',
  'май',
  'июн',
  'июл',
  'авг',
  'сен',
  'окт',
  'ноя',
  'дек',
]

class DateHelper {
  format(timestamp: number): string {
    if (!timestamp) {
      return ''
    }

    const currentDate = new Date()
    const targetDate = new Date()
    targetDate.setTime(timestamp * 1000)

    const diffDays = differenceInDays(currentDate, targetDate)
    const isCurrentYear = differenceInYears(currentDate, targetDate) === 0

    if (diffDays === 0) {
      return getTimeAsString(targetDate)
    }

    if (diffDays > 0 && isCurrentYear) {
      return `${getMonthAsString(targetDate)}, ${getTimeAsString(targetDate)}`
    }

    return `${getMonthAsString(targetDate)}, ${getYearAsString(targetDate)}`
  }
}

function getTimeAsString(date: Date): string {
  return `${date.getHours()}:${date.getMinutes()}`
}

function getMonthAsString(date: Date): string {
  return `${date.getDate()} ${month[date.getMonth()]}`
}

function getYearAsString(date: Date): string {
  return date.getFullYear().toString()
}

export default new DateHelper()
