declare class FileHelper {
    fileToBase64(file: File): Promise<string>;
}
declare const _default: FileHelper;
export default _default;
