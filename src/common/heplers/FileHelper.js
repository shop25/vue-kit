"use strict";
exports.__esModule = true;
var FileHelper = /** @class */ (function () {
    function FileHelper() {
    }
    FileHelper.prototype.fileToBase64 = function (file) {
        return new Promise(function (resolve, reject) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () { return resolve(reader.result); };
            reader.onerror = function (error) { return reject(error); };
        });
    };
    return FileHelper;
}());
exports["default"] = new FileHelper();
