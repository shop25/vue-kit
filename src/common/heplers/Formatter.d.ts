declare class Formatter {
    num2str(number: number, one: string, two: string, five: string): string;
}
declare const _default: Formatter;
export default _default;
