"use strict";
exports.__esModule = true;
var Formatter = /** @class */ (function () {
    function Formatter() {
    }
    Formatter.prototype.num2str = function (number, one, two, five) {
        number = Math.abs(number) % 100;
        var n1 = number % 10;
        if (number > 10 && number < 20) {
            return five;
        }
        if (n1 > 1 && n1 < 5) {
            return two;
        }
        if (n1 == 1) {
            return one;
        }
        return five;
    };
    return Formatter;
}());
exports["default"] = new Formatter();
