import { Notify } from 'quasar';
declare const NotifyHelper: {
    notify: Notify;
    success(message: string, config?: any): void;
    error(message: string, config?: any): void;
    serverError(): void;
};
export default NotifyHelper;
