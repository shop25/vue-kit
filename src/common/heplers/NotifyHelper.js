"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
var quasar_1 = require("quasar");
quasar_1.Notify.setDefaults({
    position: 'bottom-right',
    message: '',
    color: 'positive',
    textColor: 'white',
    timeout: 1000
});
var NotifyHelper = {
    notify: quasar_1.Notify,
    success: function (message, config) {
        if (config === void 0) { config = {}; }
        quasar_1.Notify.create(__assign({ message: message }, config));
    },
    error: function (message, config) {
        if (config === void 0) { config = {}; }
        quasar_1.Notify.create(__assign({ message: message }, { textColor: 'white', color: 'negative' }, config));
    },
    serverError: function () {
        this.error('Ошибка сервера');
    }
};
exports["default"] = NotifyHelper;
