import { Notify } from 'quasar'

Notify.setDefaults({
  position: 'bottom-right',
  message: '',
  color: 'positive',
  textColor: 'white',
  timeout: 1000,
})

const NotifyHelper = {
  notify: Notify,
  success(message: string, config: any = {}) {
    Notify.create({ message, ...config })
  },
  error(message: string, config: any = {}) {
    Notify.create({
      message,
      ...{ textColor: 'white', color: 'negative' },
      ...config,
    })
  },
  serverError() {
    this.error('Ошибка сервера')
  },
}

export default NotifyHelper
