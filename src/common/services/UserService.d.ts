import User from '../../common/dto/User';
declare const UserService: {
    getUser(): Promise<User>;
    logout(): Promise<string>;
};
export default UserService;
