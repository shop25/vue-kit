"use strict";
exports.__esModule = true;
var axios_1 = require("axios");
var UserService = {
    getUser: function () {
        return axios_1["default"].get("/api/user").then(function (response) { return response.data.user; });
    },
    logout: function () {
        return axios_1["default"].post("/logout").then(function (response) { return response.data.url; });
    }
};
exports["default"] = UserService;
