import axios from 'axios'
import User from '../../common/dto/User'

const UserService = {
  getUser(): Promise<User> {
    return axios.get(`/api/user`).then(response => response.data.user)
  },
  logout(): Promise<string> {
    return axios.post(`/logout`).then(response => response.data.url)
  },
}

export default UserService
