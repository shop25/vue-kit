import DynamicModuleFactory from '@/common/decorators/DynamicModuleFactory'
import RootStore from '@/stores/RootStore'

export default DynamicModuleFactory(RootStore)
