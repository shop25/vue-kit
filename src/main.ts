import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './quasar'
import './boot'
import RootStore from '@/stores/RootStore'

new Vue({
  router,
  store: RootStore,
  render: h => h(App),
}).$mount('#app')
