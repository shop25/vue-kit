import routes from './routes'
import Vue from 'vue'
import Router from 'vue-router'
import Default from '@/layouts/Default.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Default,
      children: routes,
    },
  ],
})
