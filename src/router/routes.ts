import Index from '@/views/Index.vue'
import { RouteConfig } from 'vue-router'
import Buttons from '@/views/Buttons.vue'
import Form from '@/views/Form.vue'
import Modals from '@/views/Modals.vue'
import ContentHeaders from '@/views/ContentHeaders.vue'
import SplitterPage from '@/views/SplitterPage.vue'
import ToolbarPage from '@/views/ToolbarPage.vue'
import LoadingPage from '@/views/LoadingPage.vue'

export const Routes = {
  index: 'index',
  buttons: 'buttons',
  form: 'form',
  modal: 'modal',
  contentHeader: 'contentHeader',
  loading: 'loading',
  splitter: 'splitter',
  toolbar: 'toolbar',
}

const routeConfigs: RouteConfig[] = [
  {
    path: '',
    name: Routes.index,
    props: true,
    component: Index,
    children: [
      {
        path: '/buttons',
        name: Routes.buttons,
        component: Buttons,
      },
      {
        path: '/form',
        name: Routes.form,
        component: Form,
      },
      {
        path: '/modal',
        name: Routes.modal,
        component: Modals,
      },
      {
        path: '/content-header',
        name: Routes.contentHeader,
        component: ContentHeaders,
      },
      {
        path: '/loading',
        name: Routes.loading,
        component: LoadingPage,
      },
      {
        path: '/splitter',
        name: Routes.splitter,
        component: SplitterPage,
      },
      {
        path: '/toolbar',
        name: Routes.toolbar,
        component: ToolbarPage,
      },
    ],
  },
]

export default routeConfigs
