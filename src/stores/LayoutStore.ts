import { VuexModule, Mutation, getModule } from 'vuex-module-decorators'
import DynamicModule from '@/decorators/DynamicModule'

@DynamicModule
export class LayoutStore extends VuexModule {
  public isShowRightSidebar: boolean = false
  public isShowLeftSidebar: boolean = true
  public rightSidebarComponent: any = null

  @Mutation
  public toggleRightSidebar(force: boolean) {
    this.isShowRightSidebar = force
  }

  @Mutation
  public toggleLeftSidebar(force: boolean) {
    this.isShowLeftSidebar = force
  }

  @Mutation
  public setRightSidebarComponent(component: any) {
    this.rightSidebarComponent = component
  }
}

export default getModule(LayoutStore)
