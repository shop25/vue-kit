module.exports = {
  // proxy API requests to Valet during development
  filenameHashing: false,
  devServer: {
    proxy: 'https://staff.loc:8888',
  },

  // output built static files to Laravel's public dir.
  // note the "build" script in package.json needs to be modified as well.
  publicPath: '/build',

  indexPath: 'index.html',

  css: {
    loaderOptions: {
      // pass options to sass-loader
      sass: {},
    },
  },

  pluginOptions: {
    quasar: {
      treeShake: true,
    },
  },

  transpileDependencies: [/[\\\/]node_modules[\\\/]quasar[\\\/]/],
}
